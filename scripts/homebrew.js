const modules = ['pf2e-playtest-data', 'hopefinder', 'starfinder-field-test-for-pf2e'];

const remaps = {
    baseWeapons: 'baseWeaponTypes'
}
const additionalCategories = {
    equipmentTraits: ['armorTraits', 'weaponTraits']
}

export function insertHomebrew() {
    Hooks.once('ready', async () => {
        for (const m of modules) {
            if (!game.modules.get(m)?.active)
                continue;

            const homebrew = await fetch(`modules/pf2e-ru/homebrew/${m}.json`).then(r => r.json());
            if (!game.i18n.translations['PF2e-ru'])
                game.i18n.translations['PF2e-ru'] = {};
            for (const kv of Object.entries(homebrew)) {
                const [category, categoryEntries] = kv;
                const categoryMap = remaps[category] ?? category;
                game.i18n.translations['PF2e-ru'][m] = foundry.utils.mergeObject(game.i18n.translations['PF2e-ru'][m] ?? {}, { [categoryMap]: categoryEntries });
                const allCategories = new Set([...(additionalCategories[category] ?? []), categoryMap]);
                Object.entries(categoryEntries).forEach(([id, e]) => {
                    for (const cat of allCategories) {
                        if (CONFIG.PF2E[cat]?.[id]) {

                            if (typeof e === 'string') {
                                CONFIG.PF2E[cat][id] = `PF2e-ru.${m}.${categoryMap}.${id}`;
                            }
                            else {
                                CONFIG.PF2E[cat][id] = `PF2e-ru.${m}.${categoryMap}.${id}.label`;
                                CONFIG.PF2E.traitsDescriptions[id] = `PF2e-ru.${m}.${categoryMap}.${id}.description`;
                            }
                        }
                    }
                })
            }
        }
    })
}